# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0004_auto_20161029_0901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='trans_amount',
            field=models.IntegerField(max_length=11),
        ),
    ]
