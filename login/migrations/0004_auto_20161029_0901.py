# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0003_transaction'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='Trans_detail',
            new_name='desc_of_item_purchased',
        ),
        migrations.AddField(
            model_name='transaction',
            name='date_of_trans',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='descr_of_trans',
            field=models.CharField(default=0, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='file_url',
            field=models.CharField(default=0, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='receiver_accountno',
            field=models.CharField(default=1, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='receiver_address',
            field=models.CharField(default=1, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='receiver_name',
            field=models.CharField(default=11, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='receiver_phoneno',
            field=models.IntegerField(default=1, max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='trans_amount',
            field=models.CharField(default=1, max_length=11),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='trans_made_by',
            field=models.CharField(default=1, max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='type_of_trans',
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='u_id',
            field=models.IntegerField(default=1, max_length=11),
            preserve_default=False,
        ),
    ]
