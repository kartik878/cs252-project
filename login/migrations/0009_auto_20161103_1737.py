# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0008_auto_20161103_1720'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='date_of_trans',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
