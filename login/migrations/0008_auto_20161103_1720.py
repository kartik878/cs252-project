# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0007_auto_20161102_1007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='date_of_trans',
            field=models.DateField(blank=True),
        ),
    ]
