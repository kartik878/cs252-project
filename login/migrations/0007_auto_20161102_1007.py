# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0006_remove_transaction_file_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='receiver_phoneno',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='trans_amount',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='u_id',
            field=models.IntegerField(),
        ),
    ]
