# log/forms.py
from django.contrib.auth.forms import AuthenticationForm
from django import forms


# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'password'}))


class TransactionForm(forms.Form):
    trans_made_by = forms.CharField(required=False,
                                    widget=forms.TextInput(attrs={
                                        'name': 'trans_made_by',
                                        'class': 'form-control',
                                    }))


    type_of_trans = forms.CharField(widget=forms.TextInput(attrs={
                                           'name': 'type_of_trans',
                                        }))

    trans_amount = forms.IntegerField(required=False,
                                      widget=forms.TextInput(attrs={
                                          'name': 'trans_amount',
                                          'class': 'form-control',
                                      }))

    date_of_trans = forms.DateTimeField(required=False,
                                        widget=forms.TextInput(attrs=
                                                                {'id': 'datepicker',
                                                                'class': 'form-control',
                                                                'name': 'date_of_trans',

                                                                }))

    desc_of_item_purchased = forms.CharField(required=False,
                                             widget=forms.TextInput(attrs={
                                                 'name': 'desc_of_item_purchased',
                                                 'class': 'form-control',
                                             }))

    name = forms.CharField(required=False,
                           widget=forms.TextInput(attrs={
                               'name': 'name',
                               'class': 'form-control',
                           }))

    accountno = forms.CharField(required=False,
                                widget=forms.TextInput(attrs={
                                    'name': 'accountno',
                                    'class': 'form-control',
                                }))
    phoneno = forms.IntegerField(required=False,
                                 widget=forms.TextInput(attrs={
                                     'name': 'phoneno',
                                     'class': 'form-control',
                                 }))
    address = forms.CharField(required=False,
                              widget=forms.TextInput(attrs={
                                  'name': 'address',
                                  'class': 'form-control',
                              }))



    form_file = forms.FileField(required=False,
        widget=forms.ClearableFileInput(attrs={
            'name':'form_file',
            'multiple':True,
            'required':False,
        }))

    descr_of_trans = forms.CharField(required=False,
                                     widget=forms.Textarea(attrs={
                                     'name': 'descr_of_trans',
                                     'class': 'form-control',
                                     'cols': 50, 'rows': 5
                                 }))
