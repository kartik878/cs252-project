from django.contrib import admin
from login.models import Transaction

class TransactionAdmin(admin.ModelAdmin):
    list_display = ("u_id", "trans_made_by", "trans_amount","date_of_trans","desc_of_item_purchased")
admin.site.register(Transaction, TransactionAdmin)
