#log/views.py
import os
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .forms import TransactionForm
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from login.models import Transaction
from django.contrib.auth.models import User
from datetime import datetime
import time
# Create your views here.
# this login required decorator is to not allow to any  
# view without authenticating
@login_required(login_url="login/")
def home(request):
     if request.user.is_superuser:
         if request.method == 'GET' and 'user_info_id' in request.GET:
             transaction = Transaction.objects.all().filter(u_id=request.GET['user_info_id'])
             user_info = User.objects.all().get(id=request.GET['user_info_id'])
             context = {"transaction": transaction, "user_info":user_info}
             return render(request, "request.html", context=context)
         else:
             user_info = User.objects.all()
             context = {"user_info": user_info}
             return render(request, "president.html", context=context)

     else:
        transaction = Transaction.objects.all().filter(u_id=request.user.id)
        context = {"transaction": transaction}
        return render(request, "home.html", context=context)



def form(request):

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TransactionForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new
            # :

            trans_made_by = form.cleaned_data['trans_made_by']
            trans_amount =  form.cleaned_data['trans_amount']
            type_of_trans = form.cleaned_data['type_of_trans']
            trans_amount = form.cleaned_data['trans_amount']
            date_of_trans = form.cleaned_data['date_of_trans']
            desc_of_item_purchased = form.cleaned_data['desc_of_item_purchased']
            receiver_name = form.cleaned_data['name']
            receiver_accountno = form.cleaned_data['accountno']
            receiver_phoneno = form.cleaned_data['phoneno']
            receiver_address = form.cleaned_data['address']
            descr_of_trans = form.cleaned_data['descr_of_trans']
            t = Transaction(u_id= request.user.id,
                            trans_made_by=trans_made_by,
                            type_of_trans=type_of_trans,
                            trans_amount=trans_amount,
                            date_of_trans=date_of_trans,
                            desc_of_item_purchased=desc_of_item_purchased,
                            receiver_name=receiver_name,
                            receiver_accountno=receiver_accountno,
                            receiver_phoneno=receiver_phoneno,
                            receiver_address=receiver_address,
                            descr_of_trans=descr_of_trans
                            )
            t.save()

            directory = 'static/images/%s/%s' %(request.user.id,Transaction.objects.latest('id').id)

           #creating the directory if not exits
            if not os.path.exists(directory):
                os.makedirs(directory)
            for count, x in enumerate(request.FILES.getlist('form_file')):
                def handle_uploaded_file(f):

                    with open('static/images/%s/%s/bill_' %(request.user.id,Transaction.objects.latest('id').id)+str(count),'wb+') as destination:

                        for chunk in f.chunks():
                            destination.write(chunk)
                handle_uploaded_file(x)
            return HttpResponseRedirect('/')
        else:
            return HttpResponse("Your form is not valid.")

    # if a GET (or any other method) we'll create a blank form
    else:
        form = TransactionForm()

    return render(request, 'form.html', {'form': form})

def graph(request):
    transaction = Transaction.objects.all().filter(u_id=request.user.id)
    username = request.user.username
    identity = request.user.id
    context = {"transaction": transaction,"username":username,"identity":identity}

    return render(request,"graph.html",context=context)


def delete(request):
    transaction_id = request.GET['transaction_id']
    Transaction.objects.filter(id=transaction_id).delete()
    return HttpResponse('OK')

def reject(request):
    transaction_id = request.GET['transaction_id']
    Transaction.objects.filter(id=transaction_id).update(status=1)
    return HttpResponse('OK')

def accept(request):
    transaction_id = request.GET['transaction_id']
    Transaction.objects.filter(id=transaction_id).update(status=2)
    return HttpResponse('OK')

def gallery(request):
    user_id = Transaction.objects.get(id=request.GET['transaction']).u_id
    path = 'static/images/%s/%s' %(user_id,request.GET['transaction'])  # insert the path to your directory
    img_list = os.listdir(path)
    context = {"user_id" : user_id, "transaction_id" : request.GET['transaction'],"images":img_list}
    return render(request,"gallery.html",context=context)


def edit(request):
	transaction_id = request.GET['update_id']
	record = Transaction.objects.get(id=transaction_id)
	if request.POST:
		form = TransactionForm(request.POST)
		if form.is_valid():
			record.trans_made_by = form.cleaned_data['trans_made_by']
			record.trans_amount = form.cleaned_data['trans_amount']
			record.type_of_trans = form.cleaned_data['type_of_trans']
			record.date_of_trans = form.cleaned_data['date_of_trans']
			record.date_of_trans = form.cleaned_data['date_of_trans']
            		record.desc_of_item_purchased = form.cleaned_data['desc_of_item_purchased']
            		record.receiver_name = form.cleaned_data['name']
            		record.receiver_accountno = form.cleaned_data['accountno']
            		record.receiver_phoneno = form.cleaned_data['phoneno']
            		record.receiver_address = form.cleaned_data['address']
            		record.descr_of_trans = form.cleaned_data['descr_of_trans']
            		record.save()
			return HttpResponseRedirect('/')
		else:
			return HttpResponse("Your Form is not valid")
	else:
		form = TransactionForm(initial={"trans_made_by": record.trans_made_by, "trans_amount": record.trans_amount, 'type_of_trans': record.type_of_trans, 'trans_amount': record.trans_amount, 'date_of_trans': record.date_of_trans.date(), 'desc_of_item_purchased': record.desc_of_item_purchased, 'name': record.receiver_name, 'accountno': record.receiver_accountno, 'phoneno': record.receiver_phoneno, 'address': record.receiver_address, 'descr_of_trans': record.descr_of_trans})
	
	return render(request, 'edit.html', {'form': form})

