from django.db import models
from datetime import datetime
# Create your models here.



class Transaction(models.Model):
   u_id = models.IntegerField()
   trans_made_by = models.CharField(max_length=256)
   type_of_trans = models.CharField(max_length=20)
   trans_amount = models.IntegerField()
   date_of_trans = models.DateTimeField(default=datetime.now, blank=True)
   desc_of_item_purchased = models.CharField(max_length=256)
   receiver_name = models.CharField(max_length=256)
   receiver_accountno = models.CharField(max_length=256)
   receiver_phoneno = models.IntegerField()
   receiver_address = models.CharField(max_length=256)
   descr_of_trans = models.CharField(max_length=256)
   status = models.IntegerField(default=0)

