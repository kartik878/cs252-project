# log/urls.py
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

# We are adding a URL called /home
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^form/', views.form, name='form'),
    url(r'^graph/', views.graph, name='graph'),
    url(r'^delete/', views.delete, name='delete'),
    url(r'^reject/', views.reject, name='reject'),
    url(r'^accept/', views.accept, name='accept'),
    url(r'^edit/', views.edit, name='edit'),
    url(r'^gallery/', views.gallery, name='gallery'),

]